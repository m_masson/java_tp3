import java.util.ArrayList;

/**
 * Classe contenant une <code>Ligne</code> de <code>Hangul</code>.
 * @author Bruno Malenfant
 *
 */
public class Ligne extends ArrayList<Hangul> {
	private static final long serialVersionUID = 1L;
	
	/**
	 * Extrait les <code>Hangul</code>s d'une cha�ne de caract�res et les places dans une
	 * nouvelle <code>Ligne</code>.
	 * @param s La cha�ne de caract�res contenant les <code>Hangul</code>s.  Ne peu pas �tre <code>null</code>.
	 * @return Une nouvelle <code>Ligne</code> contenant les <code>Hangul</code>s extrait de la 
	 * cha�ne de caract�res.
	 */
	public static Ligne valeurDe( String s ) {
		assert null != s;
		
		Ligne resultat = new Ligne();
		int taille = s.length();
		int debut = 0;
		int fin = 0;
		
		do {
			debut = fin;
			while( debut < taille && Character.isWhitespace( s.charAt( debut ) ) ) {
				++ debut;
			}
			
			fin = debut;
			while( fin < taille && ! Character.isWhitespace( s.charAt( fin ) ) ) {
				++ fin;
			}
			
			if( debut < fin ) {
				resultat.add( Hangul.valeurDe( s.substring( debut, fin ) ) );
			}
		} while( fin < taille );
		
		return resultat;
	}
}
