import java.util.Scanner;

public class Principal {
	public static void main( String [] args ) {
		Scanner sc = new Scanner( System.in );
		
		System.out.print( Texte.MSSG_DEMANDE_NOM_FICHIER );
		
		String nomFichierEntrees = sc.nextLine();
		
		sc.close();
		
		TexteCoreen texte = new TexteCoreen( nomFichierEntrees );
		texte.sauveHtml( Constantes.NOM_FICHIER_SORTIE );
	}
}
