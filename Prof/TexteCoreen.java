import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Cette classe contient un texte cor�en complet.
 * (sur plusieurs <code>Ligne</code>s.)
 * @author Bruno Malenfant
 *
 */
public class TexteCoreen extends ArrayList< Ligne > {
	private static final long serialVersionUID = 1L;
	
	private static final String DEBUT_HTML = 
			"<!doctype html>\n<html>\n\t<head>\n\t\t<title>Tp2</title>\n\t</head>\n\t<body>\n\t\t<hr>\n\t\t<table>";
	private static final String FIN_HTML = 
			"\t\t</table>\n\t\t<hr>\n\t</body>\n</html>";
	private static final String DEBUT_LIGNE =
			"\t\t\t<tr>";
	private static final String FIN_LIGNE =
			"</tr>";
	private static final String DEBUT_CASE =
			"<td>";
	private static final String FIN_CASE =
			"</td>";
	private static final String CASE_VIDE =
			"<td width=\"15\"/>";
	private static final String DEBUT_ENTITE =
			"&#";
	private static final String FIN_ENTITE =
			";";
	
	/**
	 * Ce constructeur construit une nouvelle instance � l'aide du texte contenu
	 * dans un fichier.  Le texte est �crit en cor�en romanis�.
	 * 
	 * @param nomFichier Le nom du fichier contenant le texte � lire.  Ne peu pas �tre <code>null</code>.
	 */
	public TexteCoreen( String nomFichier ) {
		assert null != nomFichier;
		
		try {
			BufferedReader lecteur = new BufferedReader( new FileReader( nomFichier ) );
			
			while( lecteur.ready() ) {
				add( Ligne.valeurDe( lecteur.readLine() ) );
			}
			
			lecteur.close();
			
		} catch( FileNotFoundException e ) {
			Erreur.FICHIER_NON_TROUVE.lancer( nomFichier );
			
		} catch( IOException e ) {
			Erreur.PROBLEME_DE_LECTURE.lancer();
		}
	}
	
	/**
	 * Cette m�thode affiche la page HTML contenant le
	 * texte cor�en �crit avec les caract�res cor�en.
	 * 
	 * @param sortie Le canal sur lequel la page HTML sera affich�e.
	 */
	public void versHtml( PrintWriter sortie ) {
		sortie.println( DEBUT_HTML );
		
		boolean ligneNonVide = true;
		int i = 0;
		while( ligneNonVide ) {
			ligneNonVide = false;
			
			sortie.print( DEBUT_LIGNE );
			
			for( int j = size() - 1; 0 <= j; -- j ) {
				Ligne ligne = get( j );
				if( i < ligne.size() ) {
					sortie.print( DEBUT_CASE );
					sortie.print( DEBUT_ENTITE );
					sortie.print( ligne.get(i).unicode() );
					sortie.print( FIN_ENTITE );
					sortie.print( FIN_CASE );
					
					ligneNonVide = true;
				} else {
					sortie.print( CASE_VIDE );
				}
			}
			
			sortie.println( FIN_LIGNE );
			
			++ i;
		}
		
		sortie.println( FIN_HTML );
	}
	
	/**
	 * Construit un fichier HTML contenant le texte �crit � l'aide des 
	 * caract�res cor�en.
	 * @param nomFichier Le nom du fichier o� le texte sera �crit.  Ne peu pas �tre <code>null</code>.
	 */
	public void sauveHtml( String nomFichier ) {
		assert null == nomFichier;
		
		PrintWriter sortie;
		try {
			sortie = new PrintWriter( new BufferedWriter( new FileWriter( nomFichier ) ) );
			versHtml( sortie );
			sortie.close();
		} catch( IOException e ) {
			Erreur.FICHIER_SORTIE.lancer( nomFichier );
		}
	}
}
