import java.util.Arrays;
import java.util.List;

/**
 * Cette classe contient les consonnes finales possibles en Cor�en.
 * 
 * @author Bruno Malenfant
 *
 */
public enum ConsonneFinale {
	rien, G, GG, GS, N, NJ, NH, D, L, LG, LM, LB, LS, LT, LP, LH, M, B, BS, S, 
	SS, NG, J, C, K, T, P, H
	
	;
	
	public static final int NOMBRE = 28;
	
	/*
	 * Utilis� pour la v�rification des consonnes.
	 */
	private static List<Character> LEGAUX = 
		Arrays.asList( 'B', 'C', 'D', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'S', 'T' );
	
	/**
	 * V�rifie qu'un charact�re pourrait appartenir � une consonne finale.
	 * @param c Le caract�re � v�rifier
	 * @return <code>true</code> si le caract�re pourrait appartenir � une consonne finale,
	 *         <code>false</code> sinon.
	 */
	public static boolean estLegal( char c ) {
		return LEGAUX.contains( c );
	}
	
	/**
	 * Donne la valeur num�rique correspondante � la consonne pour fin
	 * de traduction en Unicode.
	 * @return La valeur num�rique correspondante � la consonne.
	 */
	public int numerique() {
		return super.ordinal();
	}
	
	/**
	 * Trouve la valeur �num�r� correspondante � la cha�ne de caract�re.
	 * @param s Une cha�ne de caract�re pouvant contenir une consonne finale.
	 * @return La valeur �num�r� correspondante.
	 * Si la cha�ne de caract�re n'est pas une consonne finale possible, 
	 * alors une erreur est affich�e et le programme termine.
	 */
	public static ConsonneFinale valeurDe( String s ) {
		assert null != s;
		ConsonneFinale resultat = rien;
		
		if( 0 != s.length() ) {
			try{
				resultat = Enum.valueOf( ConsonneFinale.class, s );
			} catch( IllegalArgumentException e ) {
				Erreur.CONSONNE_FINALE_INVALIDE.lancer( s );
			}
		}
		
		return resultat;
	}
}
