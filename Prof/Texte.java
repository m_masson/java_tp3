
public class Texte {
	public static final String MSSG_ERREUR = "Erreur : ";
	public static final String MSSG_ERREUR_UNE_CONSONNE = "Seulement une consonne initiale : ";
	public static final String MSSG_ERREUR_VOYELLE_NON_PRESENTE = "N'a pas trouve de voyelle : ";
	public static final String MSSG_ERREUR_CONSONNE_INITIALE_INVALIDE = "Consonne initiale invalide : ";
	public static final String MSSG_ERREUR_VOYELLE_INVALIDE = "Voyelle invalide : ";
	public static final String MSSG_ERREUR_CONSONNE_FINALE_INVALIDE = "Consonne finale invalide : ";
	public static final String MSSG_ERREUR_CHAINE_VIDE = "La chaine representant un Hangul est vide.";
	public static final String MSSG_ERREUR_FICHIER_NON_TROUVE = "Fichier inexistant.";
	public static final String MSSG_ERREUR_PROBLEME_DE_LECTURE = "Un Probleme est survenu lors de la lecture du fichier.";
	public static final String MSSG_ERREUR_FICHIER_SORTIE = "Impossible d'ouvrir le fichier de sortie.";
	
	public static final String MSSG_DEMANDE_NOM_FICHIER = "Entrez le nom du fichier a utiliser : ";
}
