package com.composition;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;

/**
 * Contient l'information pour accéder au différents groupe de voyelle.
 * 
 * @see GroupeUn
 * @see GroupeDeux
 * @see GroupeTrois
 * @see GroupeQuatre
 * @see GroupeCinq
 */
public abstract class Groupe {

    /**
     * 
     * @param entry <code>Entry</code> d'un hashmap
     * @return Retourne -1 si l'entree ne correspond a aucun element des listes 
     *          legaux des groupes.
     */
    public int addOccurence(Map.Entry entry) {
        return -1;
    }

    abstract void setOccurence(Map.Entry<Voyelle,Integer> entry);
    abstract int getOccurence();
    abstract void map(Map.Entry<Voyelle, Integer> entry);
    abstract String printHash();
    abstract void trierHash();
    abstract Voyelle getElementArrayList(int index);
    abstract ArrayList<Voyelle> getArrayList();
    abstract void ajouterVoyelle(Voyelle voy);
    abstract HashMap<Voyelle, Integer> getHashMap();

}