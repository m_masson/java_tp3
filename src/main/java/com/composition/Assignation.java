package com.composition;
import java.util.*;

/**
 * Classe servant a assigner voyelle lue à son nombre d'occurence
 * dans un texte coreen lu
 * 
 */
public class Assignation extends HashMap<Voyelle, Integer> {

    HashMap<Voyelle, Integer> hash = new HashMap<>();
    ArrayList<Voyelle> Array;

    /**
     * Constructeur
     * 
     * @param array ArrayList de Voyelle coreenne
     */
    public Assignation(ArrayList<Voyelle> array) {
        this.Array = array;
        map(array);
    }

    /**
     * Map chaque voyelle d'une arraylist a son nombre d'occurence
     * 
     * @param array La arraylist
     */
    private void map(ArrayList<Voyelle> array) {
        for (Voyelle voy : array) {
            put(voy, getOrDefault(voy, 0) + 1);
        }
    }


    /**
     * Retourne la ArrayList de voyelle passer dans le constructeur
     * 
     */
    public ArrayList<Voyelle> getArrayVoyelle() {
        return this.Array;
    }

    /**
     * Formatage du retour en chaine de caractere pour l'objet <code>this</this>
     * 
     */
    public String toString() {
        return this.entrySet().toString();
    }

}