package com.composition;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import static java.util.stream.Collectors.*;
import static java.util.Map.Entry.*;

/**
 * Decrit le groupe 5 de voyelles.
 * 
 */
public class GroupeCinq extends Groupe {
    private List<String> legaux = List.of("U", "EU", "YU");
    protected int occurence = 0;
    protected ArrayList<Voyelle> arrayList;
    protected HashMap<Voyelle, Integer> hash = new HashMap<>();

    /**
     * Constructeur
     * 
     */
    public GroupeCinq(  ) {
        arrayList = new ArrayList<>();
    }

    /**
     * Ajuste l'occurence de l'objet avec la <code>Value</code> du <code>Map.Entry</code>.
     * 
     * @param entry L'ensemble Clé/Valeur d'un entree de <code>HashMap</code>
     */
    public void setOccurence(Map.Entry<Voyelle,Integer> entry) {
        if (legaux.contains(entry.getKey().toString())) {
            this.occurence += entry.getValue();
        }
    }

    /**
     * Map l'ensemble <code>Cle</code> et <code>Value</code> si la cle est parmis
     * les valeurs <code>legaux</code> du groupe.
     * 
     * @param entry L'ensemble Clé/Valeur d'un entree de <code>HashMap</code>
     */
    public void map(Map.Entry<Voyelle, Integer> entry) {
        if (legaux.contains(entry.getKey().toString())) {
            hash.put(entry.getKey(), entry.getValue());
        }
    }

    /**
     * Ajoute la voyelle au <code>ArrayList</code> de la classe
     * 
     */
    public void ajouterVoyelle(Voyelle voy) {
        arrayList.add(voy);
    }

    /**
     * Trie en ordre decroissant chaque <code>cle</code> du <code>HashMap</code>
     * 
     */
    public void trierHash() {
        HashMap<Voyelle, Integer> sorted = hash
        .entrySet()
        .stream()
        .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
        .collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2,
                LinkedHashMap::new));
        hash = sorted;
    }

    /**
     * @return Retourne l'element du <code>ArrayList</code> a l'index <code>index</code>
     */
    public Voyelle getElementArrayList(int index) {
        return this.arrayList.get(index);
    }

    /**
     * @return Retourne le ArrayList de la classe <code>this</code>
     */
    public ArrayList<Voyelle> getArrayList() {
        return this.arrayList;
    }

    /**
     * @return Retourne toutes les valeurs <code>cle</code> et <code>value</code>
     *          du HashMap de la classe en chaine de caracteres
     */
    public String printHash() {
        return this.hash.entrySet().toString();
    }

    /**
     * @return Retourne le nombre d'occurence de l'objet <code>this</code>
     */
    public int getOccurence() {
        return this.occurence;
    }

    /**
     * @return Retourne le HashMap de l'objet <code>this</code>
     */
    public HashMap<Voyelle, Integer> getHashMap() {
        return this.hash;
    }
    
}
