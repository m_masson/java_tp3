package com.composition;


import java.util.Map;
import java.util.List;


public class Composition {
  Groupe[] groupes1 = new Groupe[]{ new GroupeUn(), new GroupeDeux(), new GroupeTrois(), new GroupeQuatre(), new GroupeCinq() };
  public static List<String> premiereNotationMaj = List.of("DO", "DO#", "RE", "RE#", "MI", "FA", "FA#", "SOL", "SOL#", "LA", "LA#", "SI");
  public static List<String> premiereNotationMajMin = List.of("Do", "Do#", "Re", "Re#", "Mi", "Fa", "Fa#", "Sol", "Sol#", "La", "La#", "Si");
  public static List<String> premiereNotationMin = List.of("do", "do#", "re", "re#", "mi", "fa", "fa#", "sol", "sol#", "la", "la#", "si");
  public static List<String> deuxiemeNotation = List.of("C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B");
  String notation;
  int[] tonique = new int[5];
  int numeroNotation;
  public List<List<String>> listeNotations = List.of(premiereNotationMaj,premiereNotationMajMin,premiereNotationMin,deuxiemeNotation);

    /**
     * Constructeur
     * @param assign L'objet Assignation en parametre
     * @param notation La note choisie par l'utilisateur au debut du programme
     */
  public Composition(Assignation assign, String notation) {
    this.notation = notation;
    numeroNotation = definirNotation();
    for (Map.Entry<Voyelle, Integer> entry : assign.entrySet()) {
      for (Groupe g : groupes1) {
        g.setOccurence(entry);
        g.map(entry);
        g.trierHash();
      }
    }
    trier();
    for (Groupe g : groupes1) {
      for (Map.Entry<Voyelle, Integer> entry : g.getHashMap().entrySet()) {
        g.ajouterVoyelle(entry.getKey());
      }
    }
  }


    /**
     * Permet de definir la bonne notation a utiliser pour les accords
     *
     * @return le int representant l'index du tableau de notation a utiliser
     */
  public int definirNotation () {
    int numeroNotation = 0;
    if (premiereNotationMajMin.contains(notation)) {
      numeroNotation = 1;
    } else if (premiereNotationMin.contains(notation)) {
      numeroNotation = 2;
    } else if (deuxiemeNotation.contains(notation)) {
      numeroNotation = 3;
    }
    return numeroNotation;
  }

    /**
     * Trier les groupes de voyelles en somme d'occurrences
     */
  private void trier() {
      for (int i = 0; i < groupes1.length; i++) {
        for (int j = 0; j < groupes1.length - 1; j++) {
            if (groupes1[j].getOccurence() < groupes1[j + 1].getOccurence()) {
                Groupe temp = groupes1[j];
                groupes1[j] = groupes1[j + 1];
                groupes1[j + 1] = temp;
            }
        }
      }
  }

    /**
     * Calcul la tonique a utiliser pour le calcul des accords pour chaque groupe
     * de voyelles
     * @return le tableau des toniques
     */
  public String[] calcTonique() {
    int[] v = { 0, 5, 7, 9, 4 };
    String[] tableauToniques = new String[5];
    int vb = listeNotations.get(numeroNotation).indexOf(notation);
    tonique[0] = vb;
    for (int i = 0; i < 5; i++) {
      tonique[i] = ((vb + v[i]) % 12);
      tableauToniques[i] = listeNotations.get(numeroNotation).get(tonique[i]);
    }
    return tableauToniques;
  }
}
