package com.composition;

import java.util.HashMap;
import java.util.Arrays;

/**
 * Classe servant à définir la suite de note pour chaque Voyelle coreenne
 * 
 */
public class Accord extends HashMap<Voyelle, String> {

    Composition compo;
    String[] tableauToniques;
    String[] type = {"M", "m", "7", "m7", "M7"};
    int numeroNotation;

    /**
     * Constructeur
     * 
     * @param compo La composition du TexteCoreen lue
     * @param numeroNotation La notation choisie (Francaise ou Anglaise)
     */
    public Accord (Composition compo, int numeroNotation) {
        this.numeroNotation = numeroNotation;
        this.compo = compo;
        tableauToniques = compo.calcTonique();
        for (int i = 0; i < compo.groupes1.length; i++) {
            for (int j = 0; j <compo.groupes1[i].getArrayList().size(); j++) {
                if (j > 4) {
                    put(compo.groupes1[i].getElementArrayList(j), tableauToniques[i] + type[4] + " : " + printAccord(i, 4)) ;
                } else {
                    put(compo.groupes1[i].getElementArrayList(j), tableauToniques[i] + type[j] + " : " + printAccord(i, j)) ;
                }
            } 
        }
    }

    /**
     * Retourne en string formate la suite de note pour chaque accord
     * 
     * @param i L'index du groupe de voyelle
     * @param j L'index de la voyelle dans le groupe
     * @return Suite de note formate
     */
    public String printAccord(int i, int j) {
        int [][] tableauCalculNotes = {{4,7},{3,7},{4,7,10},{3,7,10},{4,7,11}};
        int tonique = compo.tonique[i];
        int tierce = (compo.tonique[i] + tableauCalculNotes[j][0]) % 12;
        int quinte = (compo.tonique[i] + tableauCalculNotes[j][1]) % 12;
        int septieme;
        String str = compo.listeNotations.get(numeroNotation).get(tonique)+ " " +
                compo.listeNotations.get(numeroNotation).get(tierce)
        + " " +compo.listeNotations.get(numeroNotation).get(quinte);
        if ( j > 1) {
        septieme = (compo.tonique[i] + tableauCalculNotes[j][2]) % 12;
        str = str + " " + compo.listeNotations.get(numeroNotation).get(septieme);
        }
        return str;
    }


    /**
     * Retourne l'accord pour une voyelle spécifique
     * 
     * @param voy La voyelle
     * @return La suite de note formate pour la <code>Voyelle</code>
     */
    public String getAccord(Voyelle voy) {
        return this.get(voy);
    }

    /**
     * Formatage du retour en chaine de caractere pour l'objet <code>this</this>
     * 
     */

    public String toString() {
        return this.entrySet().toString();
    }


}