package com.composition;
import java.util.Arrays;
import java.util.List;

/**
 * Cette classe contient les consonnes initiales possibles en Cor�en.
 * 
 * @author Bruno Malenfant
 *
 */
public enum ConsonneInitiale {
	G, GG, N, D, DD, R, M, B, BB, S, SS, rien, J, JJ, C, K, T, P, H
	
	;
	
	public static final int NOMBRE = 19;

	/*
	 * Utilis� pour la v�rification des consonnes.
	 */
	private static List<Character> LEGAUX = 
		Arrays.asList( 'B', 'C', 'D', 'G', 'H', 'J', 'K', 'M', 'N', 'P', 'R', 'S', 'T' );
	
	/**
	 * V�rifie qu'un charact�re pourrait appartenir � une consonne initiale.
	 * @param c Le caract�re � v�rifier
	 * @return <code>true</code> si le caract�re pourrait appartenir � une consonne initiale,
	 *         <code>false</code> sinon.
	 */
	public static boolean estLegal( char c ) {
		return LEGAUX.contains( c );
	}
	
	/**
	 * Donne la valeur num�rique correspondante � la consonne pour fin
	 * de traduction en Unicode.
	 * @return La valeur num�rique correspondante � la consonne.
	 */
	public int numerique() {
		return super.ordinal() + 1;
	}
	
	/**
	 * Trouve la valeur �num�r� correspondante � la cha�ne de caract�re.
	 * @param s Une cha�ne de caract�re pouvant contenir une consonne initiale.
	 * @return La valeur �num�r� correspondante.
	 * Si la cha�ne de caract�re n'est pas une consonne initiale possible, 
	 * alors une erreur est affich�e et le programme termine.
	 */
	public static ConsonneInitiale valeurDe( String s ) {
		assert null != s;
		
		ConsonneInitiale resultat = rien;
		
		if( 0 != s.length() ) {
			try {
				resultat = Enum.valueOf( ConsonneInitiale.class, s );
			} catch( IllegalArgumentException e ) {
				Erreur.CONSONNE_INITIALE_INVALIDE.lancer( s );
			}
		}
		
		return resultat;
	}
}
