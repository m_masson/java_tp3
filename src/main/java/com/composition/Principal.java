package com.composition;
import java.io.PrintStream;
import java.util.Scanner;

/**
 * Travail Pratique 2 dans le cadre du cours Programmation 2 - INF2120.
 * Prend en entree un fichier .txt et le transforme en texte de symboles
 * coreens en format HTML.
 *
 * @author Alexis Bouchard
 * Code permanent : BOUA07049701
 * @author Maxime Masson
 * Code permanent : MASM06079507
 * Courriel : bouchard.alexis.2@courrier.uqam.ca
 * Courriel : masson.maxime@courrier.uqam.ca
 * Cours : INF2120-10
 * @version 2019-03-29
*/

public class Principal {
	public static void main(String[] args) throws Exception {

		System.out.println(Texte.MSG_CHOIX);
		Scanner sc = new Scanner(System.in);
		String notation = sc.nextLine();
		System.out.print(Texte.MSSG_DEMANDE_NOM_FICHIER);
		String nomFichierEntrees = sc.nextLine();
		sc.close();

		Assignation map = new Assignation(
			new TexteCoreen(nomFichierEntrees).versArrayVoyelle());

		Composition compo = new Composition(map, notation);

		Accord accord = new Accord(compo, compo.numeroNotation);
		for (Voyelle voy : map.getArrayVoyelle()) {
			System.out.println(accord.getAccord(voy));
		}
	}
}
