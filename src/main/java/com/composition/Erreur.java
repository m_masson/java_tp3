package com.composition;
/**
 * Classe de gestion des erreurs.
 * 
 * @author Bruno Malenfant
 *
 */
public enum Erreur {
	CONSONNE_FINALE_INVALIDE  ( -101, Texte.MSSG_ERREUR_CONSONNE_FINALE_INVALIDE ),
	CONSONNE_INITIALE_INVALIDE( -102, Texte.MSSG_ERREUR_CONSONNE_INITIALE_INVALIDE ),
	UNE_CONSONNE              ( -103, Texte.MSSG_ERREUR_UNE_CONSONNE ),
	VOYELLE_INVALIDE          ( -104, Texte.MSSG_ERREUR_VOYELLE_INVALIDE ),
	VOYELLE_NON_PRESENTE      ( -105, Texte.MSSG_ERREUR_VOYELLE_NON_PRESENTE ),
	CHAINE_VIDE               ( -106, Texte.MSSG_ERREUR_CHAINE_VIDE ),
	FICHIER_NON_TROUVE        ( -107, Texte.MSSG_ERREUR_FICHIER_NON_TROUVE ),
	PROBLEME_DE_LECTURE       ( -108, Texte.MSSG_ERREUR_PROBLEME_DE_LECTURE ),
	FICHIER_SORTIE            ( -109, Texte.MSSG_ERREUR_FICHIER_SORTIE )
	
	;
	
	private String _mssg;
	private int _no;
	
	/**
	 * Constructeur d'erreur.
	 * @param no Une valeur n�gative repr�sentant le code d'erreur.
	 * @param mssg Un message d'erreur, ne peu pas �tre <code>null</code>.
	 */
	private Erreur( int no, String mssg ) {
		assert null != mssg;
		assert no < 0;
		
		_no = no;
		_mssg = mssg;
	}
	
	/**
	 * Affiche le message d'erreur sur la sortie d'erreur.
	 */
	public void afficher() {
		afficher( "" );
	}
	
	/**
	 * Affiche le message d'erreur sur la sortie d'erreur.
	 * Un message suppl�mentaire est affich�.
	 * 
	 * @param complement Le message suppl�mentaire.  Ne peu pas �tre <code>null</code>.
	 */
	public void afficher( String complement ) {
		assert null != complement;
		
		System.out.println( Texte.MSSG_ERREUR + _mssg + "  " + complement );		
	}
	
	/**
	 * Affiche un message d'erreur et ensuite termine l'ex�cution avec le code
	 * d'erreur associ�.
	 */
	public void lancer() {
		lancer( "" );
	}
	
	/**
	 * Affiche un message d'erreur et un message suppl�mentaire.  Ensuite, l'ex�cution
	 * du code est termin� avec le code d'erreur associ�.
	 * 
	 * @param complement Le message d'erreur suppl�mentaire.  Ne peu pas etre <code>null</code>
	 */
	public void lancer( String complement ) {
		assert null != complement;
		
		System.err.println( Texte.MSSG_ERREUR + _mssg + "  " + complement );
		System.exit( _no );
	}
}
