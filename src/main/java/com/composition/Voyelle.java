package com.composition;
import java.util.Arrays;
import java.util.List;

/**
 * Cette classe contient les voyelles possibles en Cor�en.
 * 
 * @author Bruno Malenfant
 *
 */
public enum Voyelle {
	A, AE, YA, YAE, EO, E, YEO, YE, O, WA, WAE, OE, YO, U, 
	WEO, WE, WI, YU, EU, YI, I
	
	;
	
	public static final int NOMBRE = 21;

	/*
	 * Utilis� pour la v�rification des voyelles.
	 */
	private static List<Character> LEGAUX = 
		Arrays.asList( 'A', 'E', 'I', 'O', 'U', 'W', 'Y' );
	
	/**
	 * V�rifie qu'un charact�re pourrait appartenir � une voyelle.
	 * @param c Le caract�re � v�rifier
	 * @return <code>true</code> si le caract�re pourrait appartenir � une voyelle,
	 *         <code>false</code> sinon.
	 */
	public static boolean estLegal( char c ) {
		return LEGAUX.contains( c );
	}
	
	/**
	 * Donne la valeur num�rique correspondante � la voyelle pour fin
	 * de traduction en Unicode.
	 * @return La valeur num�rique correspondante � la voyelle.
	 */
	public int numerique() {
		return super.ordinal() + 1;
	}
	
	/**
	 * Trouve la valeur �num�r� correspondante � la cha�ne de caract�re.
	 * @param s Une cha�ne de caract�re pouvant contenir une voyelle.
	 * @return La valeur �num�r� correspondante.
	 * Si la cha�ne de caract�re n'est pas une voyelle possible, 
	 * alors une erreur est affich�e et le programme termine.
	 */
	public static Voyelle valeurDe( String s ) {
		assert null != s;
		
		Voyelle resultat = null;
		
		try{
			resultat = Enum.valueOf( Voyelle.class, s ); 
		} catch( IllegalArgumentException e ) {
			Erreur.VOYELLE_INVALIDE.lancer( s );
		}
		
		return resultat;
	}
}
