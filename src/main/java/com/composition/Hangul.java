package com.composition;
/**
 * Classe servant � contenir un <code>Hangul</code> (syllable cor�enne).
 * 
 * @author Bruno Malenfant
 *
 */
public class Hangul {
	protected ConsonneInitiale ci;
	protected Voyelle v;
	protected ConsonneFinale cf;
	
	/**
	 * constantes utilis� pour la construction des Unicodes des <code>Hangul</code>s.
	 */
	private static final int COEFICIENT_V = ConsonneFinale.NOMBRE;
	private static final int COEFICIENT_C = ConsonneFinale.NOMBRE * Voyelle.NOMBRE;
	private static final int PREMIER_UNICODE = 44_032;
	
	/**
	 * Constructeur.
	 * 
	 * @param ci La consonne initiale, ne peu pas etre <code>null</code>.
	 * @param v La voyelle, ne peu pas etre <code>null</code>
	 * @param cf La consonne finale, ne peu pas etre <code>null</code>
	 */
	public Hangul( ConsonneInitiale ci, Voyelle v, ConsonneFinale cf ) {
		assert null != ci;
		assert null != v;
		assert null != cf;
		
		this.ci = ci;
		this.v = v;
		this.cf = cf;
	}
	
	/**
	 * Retourne l'unicode de l'<code>Hangul</code>.
	 * @return L'unicode de l'<code>Hangul</code>.
	 */
	public int unicode() {
		return PREMIER_UNICODE + COEFICIENT_C * ( ci.numerique() - 1 ) + COEFICIENT_V * ( v.numerique() - 1 ) + cf.numerique();
	}

	public Voyelle voyelle() {
		return this.v;
	}
	
	/**
	 * Transforme une cha�ne de caract�res en un <code>Hangul</code>.
	 *
	 * @param s La cha�ne de caract�res � tranformer.  Ne peu pas �tre <code>null</code>.
	 * @return Le <code>Hangul</code> correspondant � la cha�ne de caract�res.
	 * Si la cha�ne de caract�res ne correspond pas � un <code>Hangul</code>, alors
	 * la m�thode affiche un message d'erreur et termine l'ex�cution du logiciel.
	 */
	public static Hangul valeurDe( String s ) {
		assert null != s;
		
		if( 0 == s.length() ) {
			Erreur.CHAINE_VIDE.lancer();
		}
		
		int debutVoyelle = 0;
		
		while( debutVoyelle < s.length() && ConsonneInitiale.estLegal( s.charAt( debutVoyelle ) ) ) {
			++ debutVoyelle;
		}
		
		if( debutVoyelle == s.length() ) {
			Erreur.UNE_CONSONNE.lancer( s );
		}
		
		ConsonneInitiale ci = ConsonneInitiale.valeurDe( s.substring( 0, debutVoyelle ) );
		
		int debutConsonneFinale = debutVoyelle;
		
		while( debutConsonneFinale < s.length() && Voyelle.estLegal( s.charAt( debutConsonneFinale ) ) ) {
			++ debutConsonneFinale;
		}
		
		if( debutConsonneFinale == debutVoyelle ) {
			Erreur.VOYELLE_NON_PRESENTE.lancer( s );
		}

		Voyelle v = Voyelle.valeurDe( s.substring( debutVoyelle, debutConsonneFinale ) );
		
		ConsonneFinale cf = ConsonneFinale.valeurDe( s.substring( debutConsonneFinale ) );		

		return new Hangul( ci, v, cf );
	}

	public String toString() {
		return this.v.toString();
	}
}
